import re
import requests

from bs4 import BeautifulSoup
from datetime import date
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from smtplib import SMTP


def _collect_state_urls(s, root_url):
	
	state_urls_list = []
	r = s.get(url=root_url)
	soup = BeautifulSoup(r.text, 'lxml').body
	soup = soup.find('div', attrs={'id':"main_container"})
	soup = soup.find('div', attrs={'id':"main_container_a"})
	soup = soup.find('div', attrs={'id':"main_container_a_2"})
	soup = soup.find('div', attrs={'id':"data_box"}).table.tr
	columns = soup.find_all('td')
	
	for column in columns:
		hyperlinks = column.find_all('a')
		
		for link in hyperlinks:
			state_urls_list.append(link['href'])
	
	return(state_urls_list)


def _collect_newspaper_urls(s, state_url, newspaper_urls_list):
	
	print("Collecting newspaper URLs from %s" % state_url)
	r = s.get(url=state_url)
	soup = BeautifulSoup(r.text, 'lxml').body
	soup = soup.find('div', attrs={'id':"main_container"})
	soup = soup.find('div', attrs={'id':"main_container_a"})
	soup = soup.find('div', attrs={'id':"main_container_a_2"})
	soup = soup.find_all('div', attrs={'id':"data_box"})[1]
	strings = soup.find_all(string='\n&nbsp')
	
	for st in strings:
		newspaper_urls_list.append(st.next_sibling['href'])
	
	print("Collected %d newspaper URLs thus far." % len(newspaper_urls_list))
	return(newspaper_urls_list)
	

def _save_newspaper_URLs(file_path, newspaper_urls_list):

	with open(file_path, 'w') as fileObject:
		fileObject.truncate()
		
		for newspaper_url in newspaper_urls_list:
			fileObject.write(newspaper_url+'\n')

			
def _read_from_file(file_path):

	print("Reading newspaper URLs from file.")
	newspaper_urls_list = []
	
	with open(file_path, 'r') as fileObject:
	
		for newspaper_url in fileObject:
			newspaper_urls_list.append(newspaper_url[0:-1])
	
	return(newspaper_urls_list)


def _prep_files(captured_file_path, error_file_path, today):
	
	with open(captured_file_path, 'w') as fileObject:
		fileObject.truncate()
		fileObject.write("<html>\n<head>\n</head>\n<body>\n<h2>" + today + "</h2>\n")
	
	with open(error_file_path, 'w') as fileObject:
		fileObject.truncate()


def _prep_captured_links(newspaper_url, captured_hyperlinks, captured_strings):

	html_links = []
	num_links = len(captured_hyperlinks)
	
	for i in range(num_links):
		
		# If the link is a complete URL...
		if re.search("^https?://", captured_hyperlinks[i]) != None:
			html_links.append("<p><a href='" + captured_hyperlinks[i] + "'>" + 
							  captured_strings[i] + "</a></p>\n")
		
		# Otherwise, if the link is a fragment...
		else:
			html_links.append("<p><a href='" + newspaper_url[0:-1] + captured_hyperlinks[i] + "'>" + 
							  captured_strings[i] + "</a></p>\n")
		
	# End for loop
	# Now keep only the unique links
	return(set(html_links))
	
	
def _save_error(error_file_path, phase, newspaper_url, i, exc):

	print("Skipping due to phase %d error" % phase)
	message = "Phase %d error with %s at index %d: %s" % (phase, newspaper_url, i, exc)
	
	with open(error_file_path, 'a') as fileObject:
		fileObject.write(message + '\n')


def _save_captured_links(captured_file_path, html_entrys):

	print("Capturing hyperlink(s)!")
	
	with open(captured_file_path, 'a') as fileObject:
	
		for entry in html_entrys:
			fileObject.write(entry)

							 
def _polish_captured_file(captured_file_path):

	with open(captured_file_path, 'a') as fileObject:
		fileObject.write("</body>\n</html>")
							 

def hunt(s, newspaper_url_list, captured_file_path, error_file_path, today):
	
	print("Scanning URLs...")
	num_urls = len(newspaper_url_list)
	
	keyword = re.compile("(?:[Ii][Dd](entity)? [Tt]heft|[Dd]ata [Bb]reach)")
	
	# Prepare the captured and error files.
	_prep_files(captured_file_path, error_file_path, today)
	
	for i in range(num_urls):
	
		# PHASE ONE: access newspaper URL and retrieve HTML
		try:
			phase = 1
			print("%d of %d: %s" % (i+1, num_urls, newspaper_url_list[i]))
			r = s.get(url=newspaper_url_list[i], timeout=16)
			soup = BeautifulSoup(r.text, 'lxml').body
			
		except Exception as exc: 
			_save_error(error_file_path, phase, newspaper_url_list[i], i, exc)
		
		else:
			
			# PHASE TWO: extract all internal hyperlinks
			try:
				phase = 2
				domain = re.search("^https?://(?:www\.)?(.+)\.", newspaper_url_list[i])
				
				all_hyperlink_soup = soup.find_all('a', recursive=True)
				filtered_hyperlink_soup = []
				
				for hyperlink_soup in all_hyperlink_soup:
				
					try:
						hyperlink = hyperlink_soup['href']
						
					except KeyError:
						# some <a> tags have no href
						pass
						
					else:
						if (re.search(domain.group(1), hyperlink) != None) or \
						(re.search("^/", hyperlink) != None):
							filtered_hyperlink_soup.append(hyperlink_soup)
						
			except Exception as exc: 
				_save_error(error_file_path, phase, newspaper_url_list[i], i, exc)
			
			else:
			
				# PHASE THREE: sift for strings that contain keywords
				try:
					phase = 3
					captured_hyperlinks = []
					captured_strings = []
					
					for hyperlink_soup in filtered_hyperlink_soup:
						string_gen = hyperlink_soup.stripped_strings
						
						for string in string_gen:
						
							if re.search(keyword, string.encode('utf-8')) != None:
								captured_hyperlinks.append(hyperlink_soup['href'])
								captured_strings.append(string.encode('utf-8'))
								
					if len(captured_hyperlinks) > 0:
						html_entrys = _prep_captured_links(newspaper_url_list[i], 
														  captured_hyperlinks, captured_strings)
						_save_captured_links(captured_file_path, html_entrys)
							
				except Exception as exc:
					_save_error(error_file_path, phase, newspaper_url_list[i], i, exc)

	# End for-loop
	_polish_captured_file(captured_file_path)
	

def email_out(captured_file_path, today, recipient):
	
	from_usr = "spiderbyte0000@gmail.com"
	pwd = "cakeblog0"
	to_usr = recipient

	email = MIMEMultipart()
	email['Subject'] = "Scraper results"
	email['From'] = from_usr
	email['To'] = to_usr

	body = MIMEText("Captured hyperlinks on %s" % today, _subtype='plain')
	email.attach(body)

	with open(captured_file_path, 'r') as fileObject:
		attachment = MIMEText(fileObject.read(), _subtype="html")

	attachment.add_header('content-disposition', 'attachment', filename='captured.html')
	email.attach(attachment)

	print ("Emailing captured.html to %s" % to_usr)
	server = SMTP(host="smtp.gmail.com", port=587)
	server.ehlo()
	server.starttls()
	server.login(from_usr, pwd)
	server.sendmail(from_usr, to_usr, email.as_string())
	server.quit()
	
	
def main(folder_path, start = 1):
	"""
	This is the main function.
	"""

	s = requests.Session()
	my_headers = {'user-agent':'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36'}
	s.headers.update(my_headers)
	
	# Scrape for the newspaper URLs.
	if start == 0:
		
		root_url = "http://www.usnpl.com/"
		print("Accessing %s" % root_url)
		
		# The newspapers are partitioned according to state. Get the state-specific URLs.
		state_urls_list = _collect_state_urls(s, root_url)
		
		# Visit each state URL and extract the newspaper URLs therein.
		newspaper_urls_list = []
		for state_url in state_urls_list:
			newspaper_urls_list = _collect_newspaper_urls(s, state_url, newspaper_urls_list)
		
		# Save the newspaper URLs to a file.
		_save_newspaper_URLs(folder_path+'newspaper_urls.txt', newspaper_urls_list)
	
	# Read from file for the newspaper URLs 
	elif start == 1:
		newspaper_urls_list = _read_from_file(folder_path+'modified_newspaper_urls.txt')
	
	else:
		raise Exception("start parameter takes either 0 or 1, received %r" % start)
	
	# Hunt down articles on ID theft or data breaches in each URL
	captured_file_path = folder_path+'captured.html'
	error_file_path = folder_path+'errors.txt'
	today = date.today().isoformat()
	hunt(s, newspaper_urls_list, captured_file_path, error_file_path, today)
	
	# Email the captured.html file
	email_out(captured_file_path, today, "annekemarshall@gmail.com")
	print("Operation complete")


if __name__ == "__main__":

	main("C:/Users/Nathan/Desktop/IDtheft/")